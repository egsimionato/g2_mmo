package qb9.galileo.domain.model;

public class Location {

    private Position position;

    private Direction direction;
    private int mod;

    public Location(Position position) {
        this.position = position;
        this.direction = Direction.EAST;
        this.mod = 0;
    }

    public int getX() { return position.getX(); }
    public int getY() { return position.getY(); }
    public Direction getDirection() { return direction; }
    public int getMod() { return mod;  }
}

