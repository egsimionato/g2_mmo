package qb9.galileo.domain.model;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;

public class LocationTest {

    private Location location;
    private final int x = 12;
    private final int y = 32;

    @Before
    public void beforeTest() {
        location = new Location(new Position(x, y));
    }

    @Test
    public void whenInstantiateThenLocationIsSet() {
    }

    @Test
    public void whenInstantiateThenXIsStored() {
        assertEquals(location.getX(), x);
    }

    @Test
    public void whenInstantiateThenYIsStored() {
        assertEquals(location.getY(), y);
    }


}

public class LocationSpec extends spock.lang.Specification {

    def "When instantiate then location and direction is set"() {
        when: "a location instantiate"
        def location = new Location(new Position(10, 10));

        then: "x is stored"
        location.x == 10;
        and: "y is stored"
        location.y == 10;
        and: "direction is stored"
        location.direction == Direction.EAST;
        location.mod == 0
    }

}
